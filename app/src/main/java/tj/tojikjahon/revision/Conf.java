package tj.tojikjahon.revision;

import java.util.Arrays;
import java.util.List;


public final class Conf {
    public static final String IP = "http://217.8.34.72/";
    public static final String API_URL = IP + "pos_client/api/";
    public static final String USER_NAME = "zavsklad";

    public static List<String> storesValues = Arrays.asList("Худжанд", "Худжанд2", "Сомон", "Душанбе");
    public static List<String> storesKeys = Arrays.asList("5", "21", "12", "8");

    public static List<String> categoriesValues = Arrays.asList("Обувь", "Одежда", "Аксессуар", "Общий");
    public static List<String> categoriesKeys = Arrays.asList("2", "1", "4", "3");

    public static String SELECTED_CATEGORY = "selected_category";
    public static String SELECTED_STORE = "selected_store";
    public static String BY_ONE = "by_one";

}
