package tj.tojikjahon.revision;

import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;


public class XmlPullParserHandler {

    List<Product> products;
    private Product product;
    private String text;
    private String attribute;
    private String TAG = this.getClass().getSimpleName();


    public XmlPullParserHandler() {
        products = new ArrayList<>();
    }

    public List<Product> getProducts() {
        return products;
    }

    public List<Product> parse(String xml) {
        XmlPullParser xpp;
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            xpp = factory.newPullParser();
            xpp.setInput(new StringReader(xml));

            int eventType = xpp.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tName = xpp.getName();

                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tName.equalsIgnoreCase("product")) {
                            product = new Product();
                            attribute = xpp.getAttributeValue(null, "id");
                            Log.d(TAG, "attribute: " + attribute);
                        }
                        break;
                    case XmlPullParser.TEXT:
                        text = xpp.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if (tName.equalsIgnoreCase("product")){
                            product.setBarcode(attribute);
                            addProduct(product);
                        } else if (tName.equalsIgnoreCase("name")) {
                            product.setArticle(text);
                        } else if (tName.equalsIgnoreCase("count")) {
                            product.setQuantityNeeded(Float.parseFloat(text));
                        } else if (tName.equalsIgnoreCase("row_id")) {
                            product.setRowId(text);
                        } else if (tName.equalsIgnoreCase("image_source")) {
                            product.setPath(text);
                        }

                        break;
                    default:
                        break;
                }
                eventType = xpp.next();
            }
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
        return products;
    }

    private void addProduct(Product product) {
        boolean inProduct = false;
        for (Product p : products) {
            if (p.getBarcode().equals(product.getBarcode())) {
                p.setQuantityNeeded(p.getQuantityNeeded() + product.getQuantityNeeded());
                inProduct = true;
                break;
            }
        }
        if (!inProduct) {
            products.add(product);
        }
    }


    public static boolean statusParser(String xml) {
        boolean state = false;
        XmlPullParser xpp;
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            xpp = factory.newPullParser();
            xpp.setInput(new StringReader(xml));

            int eventType = xpp.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tName = xpp.getName();

                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        break;
                    case XmlPullParser.TEXT:
                        if (tName.equalsIgnoreCase("status")) {
                            if (xpp.getText().equalsIgnoreCase("2")) {
                                return true;
                            }
                        }
                        break;
                    case XmlPullParser.END_TAG:

                        break;
                    default:
                        break;
                }
                eventType = xpp.next();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return state;
    }
}
