package tj.tojikjahon.revision;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;

public class SettingsFragment extends Fragment{

    private final String TAG = this.getClass().getSimpleName();
    SharedPreferences sharedPreferences;

    public SettingsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.nav_setttings, container, false);
        Spinner spinnerStores = (Spinner) rootView.findViewById(R.id.spnStores);
        Spinner spinnerCategories = (Spinner) rootView.findViewById(R.id.spnCategories);
        CheckBox chkByOne = (CheckBox) rootView.findViewById(R.id.chkByOne);

        ArrayAdapter<String> storesAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, Conf.storesValues);

        ArrayAdapter<String> categoriesAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, Conf.categoriesValues);

        sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Conf.BY_ONE, "0");
        editor.commit();

        spinnerStores.setAdapter(storesAdapter);
        spinnerStores.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Conf.SELECTED_STORE, Conf.storesKeys.get(i));
                editor.commit();

                Log.d(TAG, String.valueOf(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerCategories.setAdapter(categoriesAdapter);
        spinnerCategories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Conf.SELECTED_CATEGORY, Conf.categoriesKeys.get(i));
                editor.commit();

                Log.d(TAG, String.valueOf(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        chkByOne.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                String val = "0";
                if (b) {
                   val = "1" ;
                }
                sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Conf.BY_ONE, val);
                editor.commit();

                Log.d(TAG, val);
            }
        });

        return rootView;
    }
}
