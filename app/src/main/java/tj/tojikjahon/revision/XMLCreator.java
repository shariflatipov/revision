package tj.tojikjahon.revision;

import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;


public class XMLCreator {
    private String stock = "cash_lining";
    private String checksum = "0af8cdf53224c0f8616db9a7426c263fa39f1e0b";
    private Calendar cal = Calendar.getInstance();
    private String _stockId;
    private String _categId;
    private String _byOneArticle;
    private String _username;

    public XMLCreator(String username, String stockId, String categId, String byOneArticle) {
        _stockId = stockId;
        _categId = categId;
        _byOneArticle = byOneArticle;

       _username = username;
    }

    String generateHeader() {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
        String dt = format.format(cal.getTime());

        try {
            serializer.setOutput(writer);
            serializer.startDocument("UTF-8", true);
            serializer.startTag("", "magazin");

            serializer.startTag("", "seller");
            serializer.attribute("", "login", _username);
            serializer.attribute("", "checksum", checksum);
            serializer.attribute("", "act", "11");
            serializer.attribute("", "stock", stock);
            serializer.attribute("", "date", dt);
            serializer.endTag("", "seller");

            serializer.startTag("", "stock_id");
            serializer.text(_stockId);
            serializer.endTag("", "stock_id");

            serializer.startTag("", "categ_id");
            serializer.text(_categId);
            serializer.endTag("", "categ_id");

            serializer.startTag("", "by_one_article");
            serializer.text(_byOneArticle);
            serializer.endTag("", "by_one_article");

            serializer.endTag("", "magazin");
            serializer.endDocument();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return writer.toString();
    }


    String sendRevisedProducts(List<Product> productList) {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
        String dt = format.format(cal.getTime());

        try {
            serializer.setOutput(writer);
            serializer.startDocument("UTF-8", true);
            serializer.startTag("", "magazin");

            serializer.startTag("", "seller");
            serializer.attribute("", "login", _username);
            serializer.attribute("", "checksum", checksum);
            serializer.attribute("", "act", "12");
            serializer.attribute("", "stock", stock);
            serializer.attribute("", "date", dt);
            serializer.endTag("", "seller");


            serializer.startTag("", "stock_id");
            serializer.text(_stockId);
            serializer.endTag("", "stock_id");

            serializer.startTag("", "categ_id");
            serializer.text(_categId);
            serializer.endTag("", "categ_id");

            serializer.startTag("", "by_one_article");
            serializer.text(_byOneArticle);
            serializer.endTag("", "by_one_article");

            serializer.startTag("", "revision");
            serializer.attribute("", "date", dt);
            serializer.attribute("", "id", "0");

            for (Product p : productList) {
                serializer.startTag("", "product");
                serializer.attribute("", "id", p.getBarcode());

                serializer.startTag("", "row_id");
                serializer.text(p.getRowId());
                serializer.endTag("", "row_id");

                serializer.startTag("", "count");
                serializer.text(String.valueOf(p.getQuantityFact()));
                serializer.endTag("", "count");

                serializer.endTag("", "product");
            }

            serializer.endTag("", "revision");

            serializer.endTag("", "magazin");
            serializer.endDocument();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return writer.toString();
    }
}
