package tj.tojikjahon.revision;

import android.util.Xml;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;


public interface ProductInterface {
     @Headers({
        "Content-type: Application/XML"
     })
     @GET("/pos_client/api/")
     Call<List<Product>> productRepo(@Body String asdf);
}
