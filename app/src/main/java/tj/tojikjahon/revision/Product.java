package tj.tojikjahon.revision;

public class Product {

    String rowId;
    String article;
    String barcode;
    String path;
    float quantityNeeded;
    float quantityFact;

    private static String TAG = "Product";

    public Product() {

    }

    public Product(String article, String barcode, float quantityNeeded,
                   float quantityFact, String imagePath) {

        this.article = article;
        this.barcode = barcode;
        this.quantityNeeded = quantityNeeded;
        this.quantityFact = quantityFact;
        this.path = imagePath;
    }

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public float getQuantityNeeded() {
        return quantityNeeded;
    }

    public void setQuantityNeeded(float quantityNeeded) {
        this.quantityNeeded = quantityNeeded;
    }

    public float getQuantityFact() {
        return quantityFact;
    }

    public void setQuantityFact(float quantityFact) {
        this.quantityFact = quantityFact;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return this.getArticle() + " " + this.getRowId() + " " + this.getBarcode();
    }
}
