package tj.tojikjahon.revision;


public class KeyValSpinnerAdapter {

    private int _key;
    private String _val;


    public KeyValSpinnerAdapter(int key, String val){
        _key = key;
        _val = val;
    }

    @Override
    public String toString() {
        return _val;
    }
}
