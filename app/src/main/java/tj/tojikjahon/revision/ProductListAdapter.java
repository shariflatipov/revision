package tj.tojikjahon.revision;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ProductListAdapter extends BaseAdapter{
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Product> objects;

    public ProductListAdapter(Context context, ArrayList<Product> products) {
        this.ctx = context;
        this.objects = products;
        lInflater = (LayoutInflater) ctx.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int i) {
        return objects.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = lInflater.inflate(R.layout.item, viewGroup, false);
        }

        Product product = (Product)getItem(i);

        ImageView imageView = (ImageView) view.findViewById(R.id.ivImage);

        String url = Conf.IP + product.getPath();

        TextView article = (TextView) view.findViewById(R.id.article);
        article.setText(product.article);

        TextView quantityFact = (TextView) view.findViewById(R.id.quantityFact);
        quantityFact.setText(String.valueOf(product.quantityFact));

        TextView quantityNeeded = (TextView) view.findViewById(R.id.quantityNeeded);
        quantityNeeded.setText(String.valueOf(product.quantityNeeded));

        if (product.quantityFact != product.quantityNeeded) {
            article.setTextColor(Color.RED);
            quantityFact.setTextColor(Color.RED);
            quantityNeeded.setTextColor(Color.RED);
        } else {
            article.setTextColor(Color.GREEN);
            quantityFact.setTextColor(Color.GREEN);
            quantityNeeded.setTextColor(Color.GREEN);
        }

        Glide.with(this.ctx)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.ic_menu_camera)
                .crossFade()
                .into(imageView);

        return view;
    }
}
