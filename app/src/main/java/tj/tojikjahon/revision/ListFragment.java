package tj.tojikjahon.revision;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class ListFragment extends Fragment {

    public ListFragment() {}

    private ListView list;
    private EditText editText;
    private Button btnSend;
    MediaType XML = MediaType.parse("application/xml; charset=utf-8");
    OkHttpClient client = new OkHttpClient();
    private String TAG = this.getClass().getSimpleName();
    private SharedPreferences sPref;
    private String store, byOne, category;
    String tmp = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list, container, false);

        list = (ListView) view.findViewById(R.id.prodList);
        editText = (EditText) view.findViewById(R.id.editText);
        btnSend = (Button) view.findViewById(R.id.btnSend);

        sPref = getActivity().getPreferences(Context.MODE_PRIVATE);

        store = sPref.getString(Conf.SELECTED_STORE, "");
        byOne = sPref.getString(Conf.BY_ONE, "");
        category = sPref.getString(Conf.SELECTED_CATEGORY, "");

        final ArrayList<Product> products = new ArrayList<>();
        final ProductListAdapter adapter = new ProductListAdapter(container.getContext(), products);

        list.setAdapter(adapter);

        try {
            XMLCreator creator = new XMLCreator(Conf.USER_NAME, store, category, byOne);
            String xml = creator.generateHeader();
            Log.d(TAG, xml);
            RequestBody body = RequestBody.create(XML, xml);
            Request request = new Request.Builder()
                    .url(Conf.API_URL)
                    .post(body)
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.d(TAG, "on response");
                    if (!response.isSuccessful()) throw new IOException(response.message());
                    String text = response.body().string();
                    Log.d(TAG, text);
                    XmlPullParserHandler handler = new XmlPullParserHandler();

                    List<Product> l = handler.parse(text);
                    products.addAll(l);

                    Log.d(TAG, String.valueOf(products.size()));
                    Thread t = (new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        adapter.notifyDataSetChanged();
                                    }
                                });
                            } catch (Exception e) {
                                Log.d(TAG, e.getMessage());
                            }
                        }
                    }));
                    t.start();
                }
            });
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }

//        editText.setInputType(InputType.TYPE_NULL);

//        editText.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View view, int j, KeyEvent keyEvent) {
//                boolean goodFounded = true;
//
//                tmp += keyEvent.getKeyCode() + ",";
//                Log.d(TAG, String.valueOf(j));
//
//                if (j == KeyEvent.KEYCODE_SPACE) {
//                    for (int i = 0; i < list.getCount(); i++) {
//                        Product product = (Product) list.getAdapter().getItem(i);
//                        if(product.getBarcode().equals(editText.getText().toString().trim())) {
//                            product.quantityFact += 1;
//                            adapter.notifyDataSetChanged();
//                            editText.setText("");
//                            return true;
//                        }
//                    }
//                    goodFounded = false;
//                    editText.setText("");
//                }
//
//                if(!goodFounded) {
//                    Toast.makeText(getActivity(),
//                            "Товар найден",
//                            Toast.LENGTH_SHORT).show();
//                }
//
//                return false;
//            }
//        });

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 15) {
                    for (int i = 0; i < list.getCount(); i++) {
                        Product product = (Product) list.getAdapter().getItem(i);
                        if(product.getBarcode().equals(editText.getText().toString().trim())) {
                            product.quantityFact += 1;
                            adapter.notifyDataSetChanged();
                            editText.setText("");
                        }
                    }
                    editText.setText("");
                }
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                dialog.setTitle("Вы уверены?");
                dialog.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            XMLCreator creator = new XMLCreator(Conf.USER_NAME, store, category, byOne);
                            String xml = creator.sendRevisedProducts(products);
                            RequestBody body = RequestBody.create(XML, xml);
                            Request request = new Request.Builder()
                                    .url(Conf.API_URL)
                                    .post(body)
                                    .build();

                            client.newCall(request).enqueue(new Callback() {
                                @Override
                                public void onFailure(Call call, IOException e) {
                                    e.printStackTrace();
                                }

                                @Override
                                public void onResponse(Call call, Response response) throws IOException {
                                    if (!response.isSuccessful())
                                        throw new IOException(response.message());
                                    String text = response.body().string();
                                    if (XmlPullParserHandler.statusParser(text)) {
                                        Toast.makeText(getContext(),
                                                "Данные успешно сохранены",
                                                Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(getContext(),
                                                "Ошибка при сохранении данных",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        } catch (Exception exception) {
                            Toast.makeText(getContext(), exception.getMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                dialog.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.d(TAG, "No clicked");
                    }
                });

                AlertDialog alert = dialog.create();
                alert.show();
            }
        });

        return view;
    }
}
